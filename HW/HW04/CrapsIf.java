/*
Ely Brown
HW04 pt.1
*/

import java.util.Scanner;

public class CrapsIf{
  public static void main (String [] args){
    Scanner scnr = new Scanner(System.in);
    int die1 = 0;
    int die2 = 0;
    String name = "place holder";
    int dieAdd = 0;
    System.out.println("Would you like to input your own dice? (true/false) (if no, the program will generate die rolls for you) ");
    boolean yesNo = scnr.nextBoolean();
    System.out.println("You said " + yesNo);
    if (yesNo){
      System.out.println("Input your first dice roll:");
      die1 = scnr.nextInt();
      System.out.println("Input your Second dice roll:");
      die2 = scnr.nextInt();
    }else if (!yesNo){
      die1 = (int) (Math.random() * 6 + 1);
      die2 = (int) (Math.random() * 6 + 1);
    }
    if (die1 == die2){
      if (die1 == 1){
        name = "Snake Eyes";
      }else if (die1 == 2){
        name = "Hard Four";
      }else if (die1 == 3){
        name = "Hard Six";
      }else if (die1 == 4){
        name = "Hard Eight";
      }else if (die1 == 5){
        name = "Hard Ten";
      }else if (die1 == 6){
        name = "Boxcars";
      }
    }else{
      dieAdd = die1 + die2;
      if (dieAdd == 3){
        name = "Ace Duce";
      }else if (dieAdd == 4){
        name = "Easy Four";
      }else if (dieAdd == 5){
        name = "Fever Five";
      }else if (dieAdd == 6){
        name = "Easy Six";
      }else if (dieAdd == 7){
        name = "Seven Out";
      }else if (dieAdd == 8){
        name = "Easy Eight";
      }else if (dieAdd == 9){
        name = "Nine";
      }else if (dieAdd == 10){
        name = "Easy Ten";
      }else if (dieAdd == 11){
        name = "Yo-leven";
      }
    }
    System.out.println("You rolled " + die1 + " and " + die2 + " giving you " + name);
  }
}