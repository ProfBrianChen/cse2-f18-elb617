/*
Ely Brown
HW04 pt.1
*/

import java.util.Scanner;

public class CrapsSwitch{
  public static void main (String [] args){
    Scanner scnr = new Scanner(System.in);
    int die1 = 0;
    int die2 = 0;
    String name = "place holder";
    int dieAdd = 0;
    int dieSub = 0;
    System.out.println("Would you like to input your own dice? (true = 1 /false = 0) (if no, the program will generate die rolls for you) ");
    int yesNo = scnr.nextInt();
    // All comments are the same as the other Craps file
    switch (yesNo){
      case 1:
        System.out.println("Input your first dice roll:");
        die1 = scnr.nextInt();
        System.out.println("Input your Second dice roll:");
        die2 = scnr.nextInt();
        break;
      default:
        die1 = (int) (Math.random() * 6 + 1);
        die2 = (int) (Math.random() * 6 + 1);
        break;
    }
    dieAdd = die1 + die2;
    dieSub = die1 - die2;
    switch (dieSub){
      case 0:
        switch (die1){
          case 1:
            name = "Snake Eyes";
            break;
          case 2:
            name = "Hard Four";
            break;
          case 3:
            name = "Hard Six";
            break;
          case 4:
            name = "Hard Eight";
            break;
          case 5:
            name = "Hard Ten";
            break;
          case 6:
            name = "Boxcars";
            break;
        }
        break;
      default:
        switch (dieAdd){
          case 3:
            name = "Ace Duce";
            break;
          case 4:
            name = "Easy Four";
            break;
          case 5:
            name = "Fever Five";
            break;
          case 6:
            name = "Easy Six";
            break;
          case 7:
            name = "Seven Out";
            break;
          case 8:
            name = "Easy Eight";
            break;
          case 9:
            name = "Nine";
            break;
          case 10:
            name = "Easy Ten";
            break;
          case 11:
            name = "Yo-leven";
            break; 
        }
        break;
    }
    System.out.println("You rolled " + die1 + " and " + die2 + " giving you " + name);
  }
}