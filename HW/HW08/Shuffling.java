/*
Ely Brown
Hw 08
*/


import java.util.Scanner;
public class Shuffling{ 
  public static void getHand(String[] card, int index, int cardCount, String[] hand){
      if(index - cardCount < 0){
        shuffle(card);
        index = 51;
      }
    int a = 0;
    for(int i = index; i > index - cardCount; i--){
      hand[a] = card[i];
      ++a;
    }
  }
  public static void shuffle(String[] cardOrder){
    int card1 = (int)(Math.random() * 52);
    int card2 = (int)(Math.random() * 52);
    String placeCard = "PLACEHOLDER";
    for(int i = 0; i < (int)(Math.random() * 52) + 50; i++){
      while (card1 == card2){
        card2 = (int)(Math.random() * 52);
      }
      placeCard = cardOrder[card1];
      cardOrder[card1] = cardOrder[card2];
      cardOrder[card2] = placeCard;
      card1 = (int)(Math.random() * 52);
      card2 = (int)(Math.random() * 52);
    }
  }
  public static void printArray(String[] cardList){
    for(int i = 0; i < 52; i++){
      System.out.print(cardList[i] + ", ");
    }
    System.out.println();
    System.out.println();
  }
  public static void printArrayHand(String[] cardList){
    for(int i = 0; i < 5; i++){
      System.out.print(cardList[i] + ", ");
    }
    System.out.println();
  }
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
      getHand(cards,index,numCards, hand); 
      printArrayHand(hand);
      index = index - numCards;
      if(index < 0){
        index = 51;
      }
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
}
