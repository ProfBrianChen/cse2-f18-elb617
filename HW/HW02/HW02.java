/*
Ely Brown
CSE HW 02
09/09/18
*/

public class HW02 {
  public static void main (String [] args){
    //These are the known elements
    //Amounts and costs
    int numPants = 3;
    double pantsPrice = 34.98;
    int numShirts = 2;
    double shirtPrice = 24.99;
    int numBelts = 1;
    double beltPrice = 33.99;
    double paSalesTax = 0.06;
    double pantsCost;
    double shirtCost;
    double beltCost;
    double totalCost;
    double tax;
    //Total cost of each kind of item (i.e. total cost of pants, etc)
    pantsCost = pantsPrice * numPants;
    shirtCost = shirtPrice * numShirts;
    beltCost = beltPrice * numBelts;
    totalCost = beltCost + shirtCost + pantsCost;
    System.out.println("The pants will cost $" + pantsCost);
    System.out.println("The shirts will cost $" + shirtCost);
    System.out.println("The belts will cost $" + beltCost);
    //Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
    pantsCost *= paSalesTax * 100;
    shirtCost *= paSalesTax * 100;
    beltCost *= paSalesTax *100;
    pantsCost = (int) pantsCost;
    shirtCost = (int) shirtCost;
    beltCost = (int) beltCost;
    pantsCost = pantsCost / 100.0;
    shirtCost = shirtCost / 100.0;
    beltCost = beltCost / 100.0;
    tax = (int) ((beltCost + shirtCost + pantsCost) * 100);
    System.out.println("The pants tax will cost $" + pantsCost);
    System.out.println("The shirts tax will cost $" + shirtCost);
    System.out.println("The belts tax will cost $" + beltCost);
    //Total cost of purchases (before tax)
    System.out.println("The total before tax will cost $" + totalCost);
    //Total sales tax
    System.out.println("The total tax will cost $" + (tax / 100.0));
    //Total paid for this transaction, including sales tax. 
    System.out.println("You will have to pay $" + (totalCost + (tax / 100.0)));
  }
}