/*
Ely Brown
HW 03
*/

import java.util.Scanner;

public class Pyramid{
  public static void main (String [] args){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Input the length of the pyramid: ");
    int side = scnr.nextInt();
    System.out.println("Input the height of the pyramid: ");
    int height = scnr.nextInt();
    double volume = Math.pow(side, 2) * height / 3;
    System.out.println("The volume of the pyramid is " + volume + " cubic units");
  }
}