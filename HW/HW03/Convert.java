/*
Ely Brown
HW 03
*/

import java.util.Scanner;

public class Convert{
  public static void main (String [] args){
    Scanner scnr = new Scanner(System.in);
    System.out.println("Input the amount of area in acres: ");
    double acres = scnr.nextDouble();
    System.out.println("Input the amount of rain in inches: ");
    double rain = scnr.nextDouble();
    double rainInMiles = rain / 12 / 5280;
    double squareMiles = acres / 640;
    double volume = rainInMiles * squareMiles;
    System.out.println("The volume of rain is " + volume + " cubic miles");
  }
}