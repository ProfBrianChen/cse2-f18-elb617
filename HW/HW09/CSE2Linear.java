/*
Ely Brown
Lab 09
*/

import java.util.*;

public class CSE2Linear{
  public static void scramble(int[] x){
    int y = -1;
    int p = -1;
    for(int i = 0; i < 15; i++){
      y = x[i];
      p = (int)(Math.random() * 15);
      x[i] = x[p];
      x[p] = y;
    }
    System.out.println("Scrambled: ");
    for(int i: x){
      System.out.print(i + ", ");
    }
    System.out.println();
  }
  public static void main(String[]args){
    int[] grades = new int[15];
    int min = 0;
    int max = 100;
    int num = -1;
    Scanner scnr = new Scanner(System.in);
    System.out.println("Please input 15 intigers between the values 0 and 100.  Each should be in ascending order.");
    for(int i = 0; i < 15; i++){
      while(true){
        if(scnr.hasNextInt()){
          grades[i] = scnr.nextInt();
          if(grades[i] >= min && grades[i] <= max){
            min = grades[i];
            break;
          }
        }else{
          scnr.nextLine();
        }
        
        System.out.println("Value is not an intiger/ not within the range 0-100/ not greater or equal to the previous value");
      }
    }
    for(int i: grades){
      System.out.print(i + ", ");
    }
    System.out.println();
    System.out.print("Please input a grade number to search for: ");
    while(true){
        if(scnr.hasNextInt()){
          num = scnr.nextInt();
          if(num >= 0 && num <= max){
            break;
          }
        }else{
          scnr.nextLine();
        }
        System.out.println("Value is not an intiger/ not within the range 0-100/ not greater or equal to the previous value");
    }
    int itteration = 0;
    int[] n = new int[3];
    n[0] = (int)(0);
    n[1] = (int)(15/2);
    n[2] = (int)(15);
    while(true){
      if(num == grades[n[1]]){
        ++itteration;
        System.out.println("The value was found after " + itteration + " itterations in position " + n[1] + 1 + ".");
        break;
      }else if(num > grades[n[1]]){
        n[0] = n[1];
        n[1] = (int)((n[0] + n[2])/2);
        ++itteration;
      }else if(num < grades[n[1]]){
        n[2] = n[1];
        n[1] = (int)((n[0] + n[2])/2);
        ++itteration;
      }
      if(itteration > 5){
        System.out.println("The value was not found after " + itteration + " itterations.");
        break;
      }
    }
    scramble(grades);
    System.out.print("Please input a grade number to search for: ");
    while(true){
        if(scnr.hasNextInt()){
          num = scnr.nextInt();
          if(num >= 0 && num <= max){
            break;
          }
        }else{
          scnr.nextLine();
        }
        System.out.println("Value is not an intiger/ not within the range 0-100/ not greater or equal to the previous value");
    }
    itteration = 0;
    boolean found = false;
    for(int i: grades){
      if(i == num){
        ++itteration;
        System.out.println("The value was found after " + itteration + " itterations in position " + itteration + ".");
        found = true;
        break;
      }
      ++itteration;
    }
    if(found == false){
      System.out.println("The value was not found after " + itteration + " itterations.");
    }
  }
}