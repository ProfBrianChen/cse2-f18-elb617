/*
Ely Brown
lab 09
*/
import java.util.Scanner;
public class RemoveElements{
  public static int[] randomInput(){
    int[] taco = new int[10];
    for(int i = 0; i < 10; i++){
      taco[i] = (int)(Math.random() * 10);
    }
    return taco;
  }
  public static int[] delete(int[] list, int pos, int[] array){
    boolean positionBreak = false;
    int[] newArray = new int[9];
    for(int i = 0; i < 10; i++){
      if(i == pos){
        positionBreak = true;
      }else if(positionBreak == false){
        newArray[i] = array[i];
      }else if(positionBreak == true){
        newArray[i-1] = array[i];
      }
    }
    return newArray;
  }
  public static int[] remove(int[] list, int target, int[] array){
    int[] check = new int[10];
    int count = 0;
    for(int i = 0; i < 10; i++){
      if(array[i] == target){
        check[i] = 1;
        ++count;
      }
    }
    int[] retArr = new int[10 - count];
    count = 0;
    for(int i = 0; i < 10; i++){
      if(check[i] == 1){
        ++count;
      }else{
        retArr[i - count] = array[i];
      }
    }
    return retArr;
  }
  public static int[] copy(int[] array){
    int[] arrow = new int[10];
    for(int i = 0; i < 10; i++){
      arrow[i] = array[i];
    }
    return arrow;
  }
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
    newArray1 = copy(num);
  	newArray1 = delete(num,index,newArray1);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
    newArray2 = copy(num);
  	newArray2 = remove(num,target,newArray2);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
