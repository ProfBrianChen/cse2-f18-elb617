/*
Ely Brown
CSE 2
Hw 06
*/

import java.util.Scanner;

public class EncryptedX{
  
  public static void main(String[]args){
    int rows = -1;
    Scanner scnr = new Scanner(System.in);
    System.out.println("Input how many rows and columns you would like to encrypt ranging from 0-100: ");
    while (true){
      if(scnr.hasNextInt()){
        rows = scnr.nextInt();
      }
      if(rows <= 100 && rows >= 0){
        break;
      }
      System.out.println("That was not an integer between 0-100.  Please input another number: ");
      scnr.nextLine();
    }
    for(int i = 1; i <= rows; i++){
      for(int j = 1; j <= rows; j++){
        if(j == i || j == rows - i + 1){
          System.out.print(" ");
        }else{
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}