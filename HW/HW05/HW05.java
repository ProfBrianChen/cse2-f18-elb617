/*
Ely Brown
Homework 5
*/

import java.util.Scanner;


public class HW05{
  public static void areEqual(int x, int y){
    while (x == y){
      x = (int)(Math.random() * 52 + 1);
    }
  }

  public static void main(String[] args){
    Scanner scnr = new Scanner(System.in);
    System.out.println("How many itterations would you like to run?");
    int itter = scnr.nextInt();
    int card1 = -1;
    int card2 = -1;
    int card3 = -1;
    int card4 = -1;
    int card5 = -1;
    int val1 = -1;
    int val2 = -1;
    int val3 = -1;
    int val4 = -1;
    int val5 = -1;
    int count = 0;
    int one = 0;
    int two = 0;
    int three = 0;
    int four = 0;
    int full = 0;
    while (count < itter){
      card5 = (int)(Math.random() * 52 + 1);
      card4 = (int)(Math.random() * 52 + 1);
      card3 = (int)(Math.random() * 52 + 1);
      card2 = (int)(Math.random() * 52 + 1);
      card1 = (int)(Math.random() * 52 + 1);
      areEqual(card1, card2);
      areEqual(card1, card3);
      areEqual(card1, card4);
      areEqual(card1, card5);
      areEqual(card2, card3);
      areEqual(card2, card4);
      areEqual(card2, card5);
      areEqual(card3, card4);
      areEqual(card3, card5);
      areEqual(card4, card5);
      val1 = card1 % 13;
      val2 = card2 % 13;
      val3 = card3 % 13;
      val4 = card4 % 13;
      val5 = card5 % 13;
        if(val1 == val2 && val3 == val4 && val4 == val5 || val1 == val3 && val2 == val4 && val4 == val5 || val1 == val4 && val3 == val2 && val2 == val5 || val1 == val5 && val3 == val4 && val4 == val2 || val2 == val3 && val1 == val4 && val4 == val5 || val2 == val4 && val1 == val3 && val3 == val5 || val2 == val5 && val1 == val4 && val4 == val3 || val3 == val4 && val1 == val2 && val2 == val5 || val5 == val3 && val1 == val4 && val4 == val2 || val4 == val5 && val1 == val2 && val2 == val3){
          full += 1;
        }else if (val1 == val2 && val2 == val3 && val3 == val4 || val1 == val5 && val5 == val3 && val3 == val4 || val1 == val2 && val2 == val5 && val5 == val4 || val1 == val2 && val2 == val3 && val3 == val5 || val5 == val2 && val2 == val3 && val3 == val4){
          four += 1;
        }else if (val1 == val2 && val2 == val3 || val1 == val4 && val4 == val3 || val1 == val5 && val5 == val3 || val1 == val2 && val2 == val4 || val1 == val2 && val2 == val5 || val1 == val2 && val2 == val3 || val5 == val2 && val2 == val3 || val5 == val2 && val2 == val4 || val5 == val4 && val4 == val3){
          three += 1;
        }else if (val1 == val2 && val3 == val4 || val1 == val2 && val3 == val5 || val1 == val2 && val5 == val4 || val1 == val3 && val2 == val4 || val1 == val3 && val2 == val5 || val1 == val3 && val5 == val4 || val1 == val4 && val2 == val5 || val1 == val4 && val5 == val3 || val1 == val4 && val2 == val3 || val1 == val5 && val2 == val4 || val1 == val5 && val2 == val3 || val1 == val5 && val3 == val4 || val5 == val2 && val3 == val4 || val5 == val4 && val2 == val3 || val5 == val3 && val2 == val4){
          two += 1;
        }else if(val1 == val2 || val1 == val3 || val1 == val4 || val1 == val5 || val3 == val2 || val4 == val2 || val5 == val2 || val3 == val4 || val3 == val5 || val4 == val5){
          one += 1;
        }
      count += 1;
    }
    double perFull = (double)full / (double)itter * 100.00;
    double perThree = (double)three / (double)itter * 100.00;
    double perTwo = (double)two / (double)itter * 100.00;
    double perFour = (double)four / (double)itter * 100.00;
    double perOne = (double)one / (double)itter * 100.00;
    System.out.println("In " + itter + " itterations: ");
    System.out.printf("Full houses came up %4.3f percent of the time \n", (float)perFull);
    System.out.printf("Four of a kind came up %4.3f percent of the time \n", (float)perFour);
    System.out.printf("Three of a kind came up %4.3f percent of the time \n", (float)perThree);
    System.out.printf("Two pairs came up %4.3f percent of the time \n", (float)perTwo);
    System.out.printf("One pair came up %4.3f percent of the time \n", (float)perOne);
  }
}