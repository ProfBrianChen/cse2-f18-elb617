/*
Ely Brown
Lab 01
8/20/18
This program prints an intro to me
*/

public class HW01{
  public static void main (String args[]){
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-E--L--B--6--1--7->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
    //This command prints what is inside the parenthesis, and then moves onto a new line.
    System.out.println();
    System.out.println("My name is Ely Brown");
    System.out.println("I like video games");
    System.out.println("I am going to be a Chemical Engineering Major");
  }
}