/*
Ely Brown
CSE 02
Lab 05
Oct 4, 2018
*/

import java.util.Scanner;

public class Loops{
  public static void main(String [] args){
    Scanner scnr = new Scanner(System.in);
    // tells if the loop needs to be repeated
    boolean loopNeeded = false;
    int courseNum = -1;
    String depName = "placeHolder";
    int numMeets = -1;
    int timeMeets = -1;
    String instructor = "placeHolder";
    int numStudents = -1;
      /*course number
      department name
      the number of times it meets in a week
      the time the class starts
      the instructor name
      the number of students*/
    do{
      System.out.println("What is the course number?");
      if(scnr.hasNextInt()){
        loopNeeded = false;
        courseNum = scnr.nextInt();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    do{
      System.out.println("What is the department Name?");
      if(scnr.hasNext()){
        loopNeeded = false;
        depName = scnr.next();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    do{
      System.out.println("How many times a week does the class meet?");
      if(scnr.hasNextInt()){
        loopNeeded = false;
        numMeets = scnr.nextInt();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    do{
      System.out.println("What time does the class start (Military Time)?");
      if(scnr.hasNextInt()){
        loopNeeded = false;
        timeMeets = scnr.nextInt();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    do{
      System.out.println("What is the instructor's name?");
      if(scnr.hasNext()){
        loopNeeded = false;
        instructor = scnr.next();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    do{
      System.out.println("How many students are there?");
      if(scnr.hasNextInt()){
        loopNeeded = false;
        numStudents = scnr.nextInt();
      }else{
        loopNeeded = true;
        scnr.next();
      }
    }while(loopNeeded);
    System.out.printf("The course number is %3.0f in the %10s department and meets %1.0f times per week.  The class starts at %4.0f with Professor %10s and %3.0f students. \n", (float)courseNum, depName, (float)numMeets, (float)timeMeets, instructor, (float)numStudents);
  }
}