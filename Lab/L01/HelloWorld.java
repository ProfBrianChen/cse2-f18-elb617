/*
Ely Brown
Lab 01
8/20/18
This program prints the words: Hello, World.
*/

public class HelloWorld{
  public static void main (String args[]){
    System.out.println("Hello, World");
    //This command prints what is inside the parenthesis, and then moves onto a new line.
  }
}