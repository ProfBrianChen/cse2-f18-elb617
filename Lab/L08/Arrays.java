/*
Ely Brown
CSE 02
Lab 08
*/



public class Arrays{
  public static void fill(int[] a){
    for(int i = 0; i < 100; i++){
      a[i] = (int)(Math.random()*100);
    }
  }
  public static void print1(int[] a){
    System.out.print("Array one holds the following numbers: ");
    for(int i = 0; i < 100; i++){
      System.out.print(a[i] + " ");
    }
    System.out.println();
  }
  public static void count(int[] a, int[] b){
    for(int i = 0; i < 100; i++){
      b[a[i]] += 1;
    }
  }
  public static void print2(int[] a){
    for(int i = 0; i < 100; i++){
      System.out.println("The value " + i + " comes up " + a[i] + " times.");
    }
  }
  public static void main(String[]args){
    int[] array1 = new int[100];
    int[] array2 = new int[100];
    fill(array1);
    print1(array1);
    count(array1, array2);
    print2(array2);
  }
}