/*
Ely Brown
CSE 02
9/06/18
*/


public class cyclometer {
  public static void main (String[] args){
    // Variables that the cyclometer measured
    int secsTrip1=480; 
    int secsTrip2=3220; 
  	int countsTrip1=1561;  
  	int countsTrip2=9037; 
    
    // Variables that are known
    double wheelDiameter = 27.0;  
  	double PI = 3.14159;
  	int feetPerMile = 5280;  
  	int inchesPerFoot = 12;   
  	int secondsPerMinute = 60;  
	  double distanceTrip1, distanceTrip2,totalDistance;

    // These calculate the distance for each of the individual trips, and then added for total distance
    distanceTrip1 = countsTrip1 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
  	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	  totalDistance = distanceTrip1 + distanceTrip2;

    // What will be printed on the screen
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");


  }
}