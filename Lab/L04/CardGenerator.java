/*
Ely Brown
Lab 04
Card Generator*/


public class CardGenerator{
  public static void main (String [] args){
    //This section picks the card itself
    int cardNumber = (int)(Math.random() * 52 + 1);
    //This finds the actual card number
    int card = cardNumber % 13;
    // Strings for later
    String cardNum = "place holder";
    String suit = "place holder";
    //This determines the suit of the card
    if (cardNumber <= 13){
      suit = "Spades";
    }else if(cardNumber <= 26){
      suit = "Clubs";
    }else if (cardNumber <= 39){
      suit = "Hearts";
    }else{
      suit = "Diamonds";
    }
    //This assigns and prints the card number and its suit
    if (card < 10 && card != 0){
      System.out.println("The picked card is the " + (card + 1) + " of " + suit);
    } else if (card == 10){
      cardNum = "Jack";
      System.out.println("The picked card is the " + cardNum + " of " + suit);
    }else if (card == 11){
      cardNum = "Queen";
      System.out.println("The picked card is the " + cardNum + " of " + suit);
    }else if (card == 12){
      cardNum = "King";
      System.out.println("The picked card is the " + cardNum + " of " + suit);
    }else if (card == 0){
      cardNum = "Ace";
      System.out.println("The picked card is the " + cardNum + " of " + suit);
    }
  }
}