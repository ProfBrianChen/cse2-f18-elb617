/*
Ely Brown
9/13/18
Lab #3
*/

import java.util.Scanner;

public class Check {
  public static void main (String [] args){
    Scanner checkScanner = new Scanner(System.in);
    System.out.println("Input the initial cost of the check: ");
    double total = checkScanner.nextDouble();
    System.out.println("Input the percentage of tip you would like to pay: ");
    double tip = checkScanner.nextDouble();
    tip = (tip/100) * total;
    System.out.println("Input the number of people eating: ");
    double people = checkScanner.nextDouble();
    total += tip;
    double costPerPerson = total / people;
    costPerPerson *= 100;
    costPerPerson = (int) costPerPerson;
    costPerPerson /= 100.0;
    System.out.println("Each person should pay: " + costPerPerson);
  }
}