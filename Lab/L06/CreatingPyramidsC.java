/*
Ely Brown
Lab 6 - Part C
Oct 11, 2018
CSE 02
*/


import java.util.Scanner;

public class CreatingPyramidsC{
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    int rows = -1;
    boolean loopNeeded = false;
    do{
      System.out.println("Please input an int between 1 and 10");
      if(scnr.hasNextInt()){
        rows = scnr.nextInt();
        if(rows <= 10 && rows >= 1){
          loopNeeded = false;
        }else{
          loopNeeded = true;
          System.out.println("INVALID INPUT");   
        }
      }else{
        scnr.nextLine();
        loopNeeded = true;
        System.out.println("INVALID INPUT");
      }
    }while(loopNeeded);
    for(int i = 1; i <= rows; i++){
      for(int count = rows; count > i ; count--){
        System.out.print("  ");
      }
      if(i != 10 && rows == 10){
          System.out.print(" ");
        }
      for(int j = 1; j <= i; j++){
        System.out.print(i - j + 1 + " ");
      }
      System.out.println(" ");
    }
  }
}