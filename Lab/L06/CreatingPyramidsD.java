/*
Ely Brown
Lab 6 - Part D
Oct 11, 2018
CSE 02
*/


import java.util.Scanner;

public class CreatingPyramidsD{
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    int rows = -1;
    boolean loopNeeded = false;
    do{
      System.out.println("Please input an int between 1 and 10");
      if(scnr.hasNextInt()){
        rows = scnr.nextInt();
        if(rows <= 10 && rows >= 1){
          loopNeeded = false;
        }else{
          loopNeeded = true;
          System.out.println("INVALID INPUT");   
        }
      }else{
        scnr.nextLine();
        loopNeeded = true;
        System.out.println("INVALID INPUT");
      }
    }while(loopNeeded);
    for(int i = 1; i <= rows; i++){
      for(int j = rows; j >= i; j--){
        System.out.print(j + " ");
      }
      System.out.println(" ");
    }
  }
}