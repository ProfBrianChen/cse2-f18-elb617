/*
Ely Brown
CSE 02
Lab 07
*/

import java.util.Scanner;
public class L07{
  //Random Number Generator
  public static int numbers(){
    int y = (int)(Math.random()*10);
    return y;
  }
  
  //Meathod holding words
  public static String verb(int x){
    String word = "yoo";
    switch (x) {
      case 1:
        word = " ran away from";
        break;
      case 2:
        word = " ate";
        break;
      case 3:
        word = " destroyed";
        break;
      case 4:
        word = " assimilated";
        break;
      case 5:
        word = " created";
        break;
      case 6:
        word = " threw";
        break;
      case 7:
        word = " coded";
        break;
      case 8:
        word = " broke";
        break;
      case 9:
        word = " ignited";
        break;
      default:
        word = " yeeted";
        break;
    }
    return word;
  }
  
 //Meathod holding words
  public static String noun1(int x){
    String word = "yoo";
    switch (x) {
      case 1:
        word = "He";
        break;
      case 2:
        word = "She";
        break;
      case 3:
        word = "They";
        break;
      case 4:
        word = "The fox";
        break;
      case 5:
        word = "We";
        break;
      case 6:
        word = "It";
        break;
      case 7:
        word = "The dog";
        break;
      case 8:
        word = "Santa";
        break;
      case 9:
        word = "God";
        break;
      default:
        word = "McYeety";
        break;
    }
    return word;
  }
  
   //Meathod holding words
  public static String noun2(int x){
    String word = "yoo";
    switch (x) {
      case 1:
        word = " toy";
        break;
      case 2:
        word = " universe";
        break;
      case 3:
        word = " world";
        break;
      case 4:
        word = " dog";
        break;
      case 5:
        word = " cat";
        break;
      case 6:
        word = " house";
        break;
      case 7:
        word = " computer";
        break;
      case 8:
        word = " fire";
        break;
      case 9:
        word = " table";
        break;
      default:
        word = " yeetus";
        break;
    }
    return word;
  }

   //Meathod holding words
  public static String adjectives(int x){
    String word = "yoo";
    switch (x) {
      case 1:
        word = " green";
        break;
      case 2:
        word = " smelly";
        break;
      case 3:
        word = " goopy";
      case 4:
        word = " electronic";
        break;
      case 5:
        word = " omnicient";
        break;
      case 6:
        word = " silky";
        break;
      case 7:
        word = " light";
        break;
      case 8:
        word = " heavy";
        break;
      case 9:
        word = " hot";
        break;
      default:
        word = " yeetish";
        break;
    }
    return word;
  }

  //Generation Class
  public static void generateSentence(int x){
    switch (x%3) {
      case 0:
        System.out.println(noun1(numbers()) + verb(numbers()) + " the" + adjectives(numbers()) + adjectives(numbers()) + noun2(numbers()) + ".");
        break;
      case 1:
        System.out.println("The way " + noun1(numbers()) + verb(numbers()) + " the" + adjectives(numbers()) + noun2(numbers()) + " made me cry!");
        break;
      case 2:
        System.out.println("Its too bad that " + noun1(numbers()) + " had" + verb(numbers()) + " the" + noun2(numbers()) + " since it left a" + adjectives(numbers()) + " atmosphere.");
        break;
    }
  }
  
  //Main Class
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    boolean initiate = true;
    System.out.println("Would you like to generate a random sentence? (true/false)");
    while(true){
      if (scnr.hasNextBoolean() == true){
        initiate = scnr.nextBoolean();
        break;
      }else{
        scnr.nextLine();
        System.out.println("Not a boolean. try again.");
      }
    }
    while(initiate){
      generateSentence(numbers());
      System.out.println("Would you like to generate another random sentence? (true/false)");
      while(true){
        if (scnr.hasNextBoolean() == true){
          initiate = scnr.nextBoolean();
          break;
        }else{
          scnr.nextLine();
          System.out.println("Not a boolean. try again.");
        }
      }
    }
  }
}